<launch>

    <!-- Update this for your particular data location -->
    <arg name="dataroot" default="/data/mbproc-live"/>

    <arg name="gui" default="True"/>
    <arg name="multibeam_topic" default="/sentry/sensors/reson_acq/multibeam"/>
    <arg name="nav_ns" default="/sentry/nav"/>

    <arg name="mapframe" value="odom_dr"/>
    <arg name="gridfile" value="$(arg dataroot)/post_bathy/sentry480_tide_1.00x1.00_BV12.grd"/>
    <arg name="nav_origin_file" default="$(arg dataroot)/sentry480_ros_org.yaml"/>
    <arg name="prior_offset_x" value="40"/>
    <arg name="prior_offset_y" value="-80"/>

    <group ns="sentry">

        <!-- We need to spin up a few nodes to successfully start tf -->

        <!-- Load our robot description into a ROS parameter -->
        <param name="robot_description" textfile="$(find sentry_config)/vehicle_model/sentry_runtime.urdf"/>
        <!-- Vehicle-wide "use-GUI" flag -->
        <param name="use_gui" value="$(arg gui)"/>
        <!-- Essential Nodes -->
        <!-- Convert joint angles into tf transforms -->
        <node name="robot_state_publisher" pkg="robot_state_publisher" type="state_publisher"/>
        <!-- This publishes joint states from a GUI.  We can just use the message in the bag file
        <node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher"/>
        -->

        <!-- Some of our nodes may require an origin -->
        <group ns="nav">
            <rosparam command="load" file="$(arg nav_origin_file)"/>
        </group>

        <group ns="multibeam">
            <!-- The core multibeam processing node -->
            <node name="mbproc" pkg="ds_multibeam" type="ds_multibeam_node" output="screen">
                <param name="pointcloud_reset_interval_sec" value="60.0"/>
                <param name="grid_update_interval_sec" value="15.0"/>
                <param name="map_frame_id" value="$(arg mapframe)"/>
                <param name="raw_multibeam_topic" value="$(arg multibeam_topic)"/>

                <param name="filter/min_altitude" value="0.0"/>
                <param name="filter/max_altitude" value="15000.0"/>
                <param name="filter/max_altitude_jump" value="15000.0"/>

                <param name="filter/min_range" value="0.0"/>
                <param name="filter/max_range" value="15000.0"/>
                <param name="filter/max_range_jump" value="15000.0"/>

                <param name="filter/min_depth" value="0.0"/>
                <param name="filter/max_depth" value="15000.0"/>
                <param name="filter/max_depth_jump" value="15000.0"/>

                <param name="filter/backup_dist" value="2.0"/>

                <param name="grid/grid_size" value="5.0"/>
                <param name="grid/grid_memory_limit_mb" value="512"/>
            </node>

            <!-- This node loads the post-processed GRD file and publishes it for visualization in rviz -->
            <node name="mbgrd_load" pkg="ds_multibeam" type="ds_load_mbgrd" output="screen">
                <param name="grdname" value="$(arg gridfile)"/>
                <!-- <param name="map_frame_id" value="$(arg mapframe)"/> -->
                <!-- We'll put the map in its own reference frame, and apply a transform to
                     correct for the USBL offset -->
                <param name="map_frame_id" value="prior_map"/>
                <param name="nav_ns" value="$(arg nav_ns)"/>
                <param name="resend_seconds" value="10"/>
            </node>

        </group>

        <!-- Publish the offset between the prior map's reference frame- which is USBL-corrected-
             and the odometry frame- which isn't -->
        <node name="prior_map_tf_broadcaster" pkg="tf2_ros" type="static_transform_publisher"
              args="$(arg prior_offset_x) $(arg prior_offset_y) 0 0 0 0 odom_dr prior_map"/>

        <!-- We need a node to actually replay the log data -->
        <node name="data_replay" pkg="rosbag" type="play" args="--clock --start=24000 $(arg dataroot)/merged_updated.bag"/>

        <!-- Visualization in RVIZ -->
        <group if="$(arg gui)">
            <node name="rviz" pkg="rviz" type="rviz"/>
        </group>
    </group>
</launch>