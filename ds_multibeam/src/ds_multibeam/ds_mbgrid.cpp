//
// Created by ivaughn on 7/9/18.
//

#include "ds_multibeam/ds_mbgrid.h"
#include <ros/ros.h>

namespace dsros {
namespace multibeam {

DsMbGrid::DsMbGrid() {
  // TODO
}

void DsMbGrid::setupParameters() {
  double grid_size   = ros::param::param<double>(ros::this_node::getName() + "/grid/grid_size", 1.0);
  double grid_memory_limit_mb = ros::param::param<double>(ros::this_node::getName() + "/grid/grid_memory_limit_mb", 128);
  double init_size   = ros::param::param<int>(ros::this_node::getName() + "/grid/init_size", 500);
  double resize_incr = ros::param::param<int>(ros::this_node::getName() + "/grid/resize_incr", 100);

  grid.reset(new GrowableGrid(grid_size));
  grid->setGridSizeLimitMb(grid_memory_limit_mb);
  grid->setInitSize(init_size);
  grid->setResizeIncrement(resize_incr);
}

void DsMbGrid::clearStats() {
  if (grid) {
    grid->clearStats();
  } else {
    ROS_ERROR_STREAM("Request to clear grid stats, but no grid yet exists!");
  }
}

const ds_multibeam_msgs::MultibeamGridStats& DsMbGrid::stats() const {
  if (grid) {
    return grid->stats();
  } else {
    ROS_ERROR_STREAM("Request for grid stats, but no grid yet exists!");
    return _empty_stats;
  }
}

void DsMbGrid::addPing(const MbPing& input) {
  if (grid) {
    grid->addPing(input);
  } else {
    ROS_ERROR_STREAM("Asked to add ping to non-existant grid");
  }
}

void DsMbGrid::fillOutput(PointCloud& grid_cloud) {
  grid_cloud.resize(0);

  if (!grid) {
    ROS_ERROR_STREAM("Asked to fill in grid cloud, but no grid available!");
    return;
  }

  grid_cloud.height = grid->Height();
  grid_cloud.width = grid->Width();
  grid_cloud.is_dense = false;
  grid_cloud.points.reserve( grid->Height() * grid->Width() );
  for (int v=0; v<grid->Height(); v++) {
    for (int u=0; u<grid->Width(); u++) {
      pcl::PointXYZ pt;
      Eigen::Vector3d xyz = grid->getCellXyz(u,v);
      pt.getVector3fMap() = xyz.cast<float>();
      grid_cloud.push_back(pt);
    }
  }
}

void DsMbGrid::fillOutput(ds_multibeam_msgs::MultibeamGrid& grid_msg) {

  // Fill in some basic stuff
  grid_msg.easting_min = grid->TopLeftEast();
  grid_msg.easting_max = grid->TopLeftEast() + grid->CellSize() * ( grid->Width() - 1 );

  grid_msg.northing_min = grid->TopLeftNorth();
  grid_msg.northing_max = grid->TopLeftNorth() + grid->CellSize() * ( grid->Height() - 1 );

  grid_msg.cells_easting = grid->Width();
  grid_msg.cells_northing = grid->Height();

  // scan through to get a depth offset first
  double min_z = std::numeric_limits<double>::quiet_NaN();
  double max_z = min_z;
  double accum_z = 0;
  double count_z = 0;
  for (int v=0; v<grid->Height(); v++) {
    for (int u=0; u<grid->Width(); u++) {
      if (!grid->cellPopulated(u,v)) {
        continue;
      }
      double cell_z = grid->getCell(u,v);
      if (! (min_z < cell_z)) { min_z = cell_z; }
      if (! (max_z > cell_z)) { max_z = cell_z; }
      accum_z += cell_z;
      count_z++;
    }
  }

  grid_msg.depth_min = min_z;
  grid_msg.depth_max = max_z;
  grid_msg.nodata_value = std::numeric_limits<float>::quiet_NaN();
  grid_msg.depth_offset = accum_z / count_z;

  // scan again to fill in our data
  grid_msg.depth_data.resize(grid->Width() * grid->Height());
  for (int v=0; v<grid->Height(); v++) {
    for (int u = 0; u < grid->Width(); u++) {
      if (grid->cellPopulated(u, v)) {
        grid_msg.depth_data[u + v*grid->Width()] = static_cast<float>(
            grid->getCell(u,v) - grid_msg.depth_offset);
      } else {
        grid_msg.depth_data[u + v*grid->Width()] = grid_msg.nodata_value;
      }
    }
  }
}


}
}