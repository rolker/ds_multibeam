//
// Created by ivaughn on 7/5/18.
//

#include "ds_multibeam/ds_mbproc_live.h"

int main(int argc, char* argv[] ) {

  dsros::multibeam::DsMbprocLive mbproc(argc, argv, "mbproc-live");
  mbproc.run();

  return 0;
}
