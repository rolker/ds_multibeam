//
// Created by ivaughn on 7/10/18.
//

#ifndef PROJECT_DS_GROWABLE_GRID_H
#define PROJECT_DS_GROWABLE_GRID_H

#include "ds_mbping.h"
#include <boost/utility.hpp>
#include <memory>
#include <ds_multibeam_msgs/MultibeamGridStats.h>

namespace dsros {
namespace multibeam {

class GrowableGrid : public boost::noncopyable {
 public:
  typedef std::shared_ptr<GrowableGrid> Ptr;
  typedef std::shared_ptr<const GrowableGrid> ConstPtr;

  GrowableGrid(double _cellSize);
  virtual ~GrowableGrid();

  void addPing(const MbPing& ping);

  int Width() const;
  int Height() const;
  double CellSize() const;
  double TopLeftNorth() const;
  double TopLeftEast() const;

  void reset();

  // accessors
  inline double getCell(int u, int v) {
    if (u >= 0 && u < _width && v >= 0 && v < _height) {
      double w = _weights[u + v*_width];
      if (w > 0) {
        return (_accum[u + v*_width] / w);
      }
    }
    return std::numeric_limits<double>::quiet_NaN();
  }

  inline bool cellPopulated(int u, int v) {
    return (u >= 0 && u < _width && v >= 0 && v < _height
        && _weights[u + v*_width] > 0);
  }

  inline Eigen::Vector3d getCellXyz(int u, int v) {
    Eigen::Vector3d uvh;
    uvh <<u, v, 0;
    if (u >= 0 && u < _width && v >= 0 && v < _height) {
      double w = _weights[u + v * _width];
      if (w > 0) {
        uvh(2) = _accum[u + v * _width] / w;
        return uvh2xyz(uvh);
      }
    }
    // the requested cell is unpopulated or invalid.  Return NaN
    uvh(0) = std::numeric_limits<double>::quiet_NaN();
    uvh(1) = std::numeric_limits<double>::quiet_NaN();
    uvh(2) = std::numeric_limits<double>::quiet_NaN();
    return uvh;
  }

  void clearStats();
  const ds_multibeam_msgs::MultibeamGridStats& stats() const;

  void setInitSize(int _init_size);
  void setResizeIncrement(int _resize_incr);
  void setGridSizeLimitMb(size_t mb);

 private:
  ds_multibeam_msgs::MultibeamGridStats _stats;

  double _cell_size;
  Eigen::Vector3d _origin;
  Eigen::Vector3d _scale;

  int _height;
  int _width;

  // Impose a memory limit on the max size of a grid, in MB
  // Resizes that would push us over this limit automatically fail
  float memory_limit_mb;
  float memory_used_mb;

  // depth accumulator and denominator
  double* _accum;
  double* _weights;

  // initial size of the grid in each dimension
  int INIT_SIZE;

  // amount of padding maintained in each dimension at resize
  // to reduce the number of resize operations.
  int RESIZE_INCREMENT;

  // convert local xyz coordiantes to grid uv cell ID
  inline Eigen::Vector3d xyz2uvh(const Eigen::Vector3d& xyz) const {
    // In general, we want:
    // c = (x - x0)/cell
    // r = (y - y0)/cell
    // h = (z - z0)
    return (xyz - _origin).array() / _scale.array();
  }
  inline Eigen::Vector3d uvh2xyz(const Eigen::Vector3d& uvh) const {
    // inverse of xyz2uvh
    // (xyz - origin) ./ scale = uvh
    // uvh .* scale = (xyz - origin)
    // xyz = uvh .* scale + origin
    return (uvh.array() * _scale.array()) + _origin.array();
  }
  inline bool resize(const Eigen::Vector3d& tgt_xyz,
        const Eigen::Vector3d& tgt_uvh);

  void updateStats();
  void updateCellsUsedStat();

};

}
}

#endif //PROJECT_DS_GROWABLE_GRID_H
