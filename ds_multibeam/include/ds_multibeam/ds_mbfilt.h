//
// Created by ivaughn on 7/9/18.
//

#ifndef PROJECT_DS_MBFILT_H
#define PROJECT_DS_MBFILT_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <ds_multibeam_msgs/MultibeamFilterStats.h>
#include "ds_mbping.h"

namespace dsros {
namespace multibeam {

class DsMbFilt {
 public:
  DsMbFilt();

  void setupParameters();
  void clearStats();
  const ds_multibeam_msgs::MultibeamFilterStats& stats() const;

  void filter(const MbPing& input, MbPing& output);

 protected:
  // filter parameters
  double filter_min_altitude;
  double filter_max_altitude;
  double filter_max_altitude_jump;

  double filter_min_range;
  double filter_max_range;
  double filter_max_range_jump;

  double filter_min_depth;
  double filter_max_depth;
  double filter_max_depth_jump;

  double filter_backup_dist;
  double filter_max_range_hist_frac;

  void _filter_inner(MbPing& ret, int start, int delta);

 private:
  ds_multibeam_msgs::MultibeamFilterStats _stats;

};

}
}

#endif //PROJECT_DS_MBFILT_H
