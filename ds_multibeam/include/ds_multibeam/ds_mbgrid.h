//
// Created by ivaughn on 7/9/18.
//

#ifndef PROJECT_DS_MBGRID_H
#define PROJECT_DS_MBGRID_H

#include "ds_mbping.h"
#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <ds_multibeam_msgs/MultibeamGrid.h>
#include <ds_multibeam_msgs/MultibeamGridStats.h>
#include "ds_growable_grid.h"

namespace dsros {
namespace multibeam {

class DsMbGrid {
 public:
  DsMbGrid();

  void setupParameters();

  void addPing(const MbPing& input);

  void fillOutput(PointCloud& grid);
  void fillOutput(ds_multibeam_msgs::MultibeamGrid& grid);

  void clearStats();
  const ds_multibeam_msgs::MultibeamGridStats& stats() const;

 protected:
  GrowableGrid::Ptr grid;
  double origin_lat, origin_lon;
  double holiday;

  const ds_multibeam_msgs::MultibeamGridStats _empty_stats;


};

}
}

#endif //PROJECT_DS_MBGRID_H
